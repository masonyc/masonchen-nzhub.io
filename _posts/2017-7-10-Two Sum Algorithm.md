---
layout: post
title: Algorithm - Two Sum
---

Given an array of integers, return indices of the two numbers such that they add up to a specific target.

You may assume that each input would have exactly one solution, and you may not use the same element twice.

![_config.yml](/images/TwoSum.PNG)

We can solve this question by using two for loops. The first loop goes through the input array and picks the first number. The second loop checks that are there any number in the list that matches the value of target - first number. If so, we return the two index used by the loops.

![_config.yml](/images/TwoSumAnswer.PNG)

The time complexity of this solution is O(n^2). Because looping through the list to find the first number is O(n) and looping through the rest of the list to find the second number is also O(n) therefore it is O(n^2).