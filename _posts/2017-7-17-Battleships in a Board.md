---
layout: post
title: Algorithm - Battleships in a Board
---

Given an 2D board, count how many battleships are in it. The battleships are represented with 'X's, empty slots are represented with '.'s. You may assume the following rules:

* You receive a valid board, made of only battleships or empty slots.
* Battleships can only be placed horizontally or vertically. In other words, they can only be made of the shape 1xN (1 row, N          columns) or Nx1 (N rows, 1 column), where N can be of any size.
* At least one horizontal or vertical cell separates between two battleships - there are no adjacent battleships.

![_config.yml](/images/BattleshipExample.PNG)

![_config.yml](/images/TwoBattleShip.PNG)

We can solve this question by looping through the nested list. Because in the rules, it needs to have an empty space('.') between two ships. So if the current ship has a value of 'X' then we have to check the left position and the position above the current ship to see they contain any values or not. If they do not have any values ('.') then we increment the count.