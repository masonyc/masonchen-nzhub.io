---
layout: post
title: Algorithm - Merge Two Linked List 
---

Today I will explain the algorithm about merging two linked list in to one linked list. Here is the question "Merge two sorted linked lists and return it as a new list. The new list should be made by splicing together the nodes of the first two lists."

![_config.yml](/images/MergeTwoLinkedList.PNG)

We can solve this question by using recursion. At first, we check if two input linked list are empty or not. If one of them is empty we return the another linked list. Then we compare the current value for the two input list. If list1's node is bigger than list2's node. We remove the current node in list2 by calling recursive method with list1's node and list2's next node. Then we return list2's current node to the caller and vice versa.  