---
layout: post
title: KnockoutJS - Multiple Bindings in one view
---

In the development of a website using KnoutoutJS, sometimes we need to have multiple view models in one page. You may ask "how are we going to achieve it? KnoutoutJS only supports one binding in one view." You are right, but there is a trick that allows you to bind multiple view models in one page.

![_config.yml](/images/kostopbinding.PNG)

We can define a custom binding called stopBinding and bind it to the part you want to skip the current binding.Like:

![_config.yml](/images/knockoutstopbinding.PNG)

 So everything in the StopBinding division will not have the binding on the current page instead you can bind another view model to this division. Like: 
 
 * ko.applyBindings(NewViewModel, document.getElementById("NewBinding"));

 Please note, the new view model will only work in the NewBinding division. Elements outside this division will still bind to the old binding.