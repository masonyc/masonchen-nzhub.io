---
layout: post
title: Algorithm - Add Numbers From Two Single-Linked List
---

You are given two non-empty linked lists representing two non-negative integers. The digits are stored in reverse order and each of their nodes contain a single digit. Add the two numbers and return it as a linked list.

You may assume the two numbers do not contain any leading zero, except the number 0 itself.

Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
Output: 7 -> 0 -> 8



![_config.yml](/images/AddTwoSum.PNG)

We can solve this question by creating another method that can take carry as params and use multiple if statement to do it. 

First IF statement is using to check the two linked list are empty or not. If they are not empty, we can add the current value of list 1, list 2 and t3(carry value from param). And divide by 10 to get the carry over value. Also we can get the single digit by using % operator. Then we set the a new List Node by passing the single digit as param and we set the next value for this new List Node by passing a recursive method with list1.next, list2.next and carry over value that we calculated before. 

Second IF statement is checking if t1 and t2 are Empty but carry over param is not empty then we create a new List Node with the carry over value and return it to the method.

Third IF statement is checking if t1, t2 are empty and carry over param is 0, then we just return NONE because if carry over value is 0 there is no point to create a new list node.

Fourth IF statement is checking if t1==None, then we sum up t2's current value and carry over value. Then we do the same thing like we did in the first IF statement. But please note the t1 param in the recursive method should be NONE.

Last IF statement is almost the same like fourth IF statement, but instead of t1==None, we have t2==None.