---
layout: post
title: Authenticating to Azure Active Directory
---

When we want to use Azure Active Directory to do authentication, we can use Azure Active Directory Authentication Library(ADAL) to do it easily.

![_config.yml](/images/AADAuthentication.PNG)

At first, we will need to initialize the AuthenticationContext object with the windows login URL(like https://login.windows.net/common) as param. AuthenticationContext class is used to get authentication tokens from Azure Active Directory. Then if there are any existing token we just use them. Otherwise, we use AcquireTokenAsync method with the cliendId and return URL that sets in the Azure AD to require a new token.