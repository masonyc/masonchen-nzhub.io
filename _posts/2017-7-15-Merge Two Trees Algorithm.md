---
layout: post
title: Algorithm - Merge Two Trees
---

Today I will explain the algorithm about merging two binary tress in to one binary tree. 

![_config.yml](/images/MergeTwoTreeQ.PNG)

![_config.yml](/images/MergeTwoTreeA.PNG)

We can solve this question by using recursion. At first, we check if two input Tree Node are empty or not. If one of them is empty we return the another Tree Node. If two inputs both have values inside it, then we set the the value of the current node to be the sum of t1's value and t2's value. And we set the left value of the current node by passing the left node of both inputs as params to the recursive method. Same thing to the right value the current node. The recursive method will do all the jobs for us to merge the trees in to one new binary tree.