---
layout: page
title: About
permalink: /about/
---

I am a graduate student from the University of Auckland with Bachelor of Science degree in computer science and information system.I am currently seeking for full-time developer job and working for MVP studio as an intern software developer.

### More Information

I like building LEGO figures in my own time because I like the process that brings many small pieces together and turns into a new figure. 
### Contact me

[mason.yunchen@gmail.com](mailto:mason.yunchen@gmail.com)